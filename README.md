#Usage

* Clone this repo
* Run  ``` node server.js ```
* Open [http://localhost:8080](http://localhost:8080) in your browser.


#Hints

## Sorting

* Complete functions ```insertionSort``` and ```bubbleSort``` in ```server.js```
* Check if all the right responses are generated for ```getSortedJSON``` at the client.
* Example of server url request : ```./getSortedJSON?alg=selection&key=id```
* In ```index.html``` use ```loadURL()``` to load server data asynchronously via ```getSortedJSON```.


## Filtering
